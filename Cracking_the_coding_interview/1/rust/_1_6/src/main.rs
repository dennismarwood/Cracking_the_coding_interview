/*
String Compression: Implement a method to perfom basic string compression using the counts of repeated characters. For example, the string aabcccccaaa would becom a2b1c5a3. If the "compressed" string would not become smaller than the original string, your method should return the original string. You can assume the string has only uppercase and lowercase letters (a-z).
*/

fn main() {
    let input: String = String::from("aabcCcccccccaaa");
    //let input: String = String::from("abcdefg");
    let mut output = Vec::new();
    let mut compressed = String::from("");
    struct Count {
        target: char,
        count: i8,
    }

    output.push(Count {target: input.chars().next().unwrap(), count: 0});

    let iter = input.chars().scan(0, |state, x| {
        if output[*state].target == x {
            output[*state].count += 1;
        }
        else {
            output.push(Count {target: x, count: 1});
            *state += 1;
        }
        Some(' ')
    });

    for _x in iter {};

    output.iter().for_each(|x| compressed.push_str(&format!("{}{}", x.target, x.count)));

    if compressed.len() < input.len() {
        println!("{}", compressed);
    }
    else {
        println!("{}", input)
    }
}
