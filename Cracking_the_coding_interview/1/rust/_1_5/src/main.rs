/*
There are three types of edits that can be performed on strings: insert a character, remove a character, or replace a character. Given two strings, write a function to to check if they are one edit (or zero edits) away.
EXAMPLE
pale, ple -> true
pales, pale -> true
pale, bale -> true
pale, bake -> false

*/
// Let's assume that we must add or remove based on the order given (otherwise you could just add or remove, not check what was needed)

fn main() {
    let mut left = "pale".chars().collect::<Vec<char>>();
    let mut right = "bake".chars().collect::<Vec<char>>();

    let differance: isize = left.len() as isize - right.len() as isize; //can't infer type here???

    //add a letter
    if differance == 0 {
        for (i, (l, r)) in left.iter().zip(right.iter()).enumerate() {
            if l != r {
                println!("Swapped {} in {:?} for {} in {:?}", r, right, l, left);
                right[i] = *l;
                break;
            }
        }
    //left larger
    } else if differance > 0{
        for (i, (l, r)) in left.iter().zip(right.iter()).enumerate() {
            if l != r {
                println!("Added {} into {:?} at {}", l, right, i);
                right.insert(i, *l);
                break;
            }
        }
    //right larger
    } else {
        for (i, (l, r)) in left.iter().zip(right.iter()).enumerate() {
            println!("{}{}", l, r);
            if l != r {
                println!("Added {} into {:?} at {}", r, left, i);
                left.insert(i, *r);
                break;
            }
        }
    }

    if left == right {
        println!("Strings are same in one or less change\n{:?}\n{:?}", left, right);
    }
    else {
        println!("Could not make strings equal in 1 move\n{:?}\n{:?}", left, right);
    }
}

//