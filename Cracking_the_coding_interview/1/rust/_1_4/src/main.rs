/*
Palindrome Permuation: Given a string, write a function to check if it is a permutation of a palindrome. A palindrome is a word or phrase that is the same forwards and backwards. A permutation is a rearrangement of letters. The palindrome does not need to be limited to just dictionary words.
Example
input: Tact Coa
Output: True (permutations: "taco cat", "atco cta", etc.)
*/
use std::process;
mod lib;
use lib::PaliPermu;
fn main() {
    const PALINDROME: &str = "Tact coa";
    //const PALINDROME: &str = " zaazbZzzbxx";
    //const PALINDROME: &str = "zz zzz";
    //const PALINDROME: &str = "abcacba";
    //const PALINDROME: &str = "a";
    //let palindrome = PALINDROME.clone().to_ascii_lowercase();

    let pali_permu = PaliPermu::new(PALINDROME.to_string()).unwrap_or_else(|err| {
        eprintln!("Failed to process palindrome: {}", err);
        process::exit(1);
    });

    print!("{}", pali_permu);

    for (i, x) in pali_permu.enumerate() {
        let permus = ||{
            let mut z = x.iter().map(|x| x.clone()).collect::<String>();
            if x.len() == 2 { z.insert_str(z.len()/2, "  -AND-  ");}
            z
        };

        println!("{} - {}", i, permus());
    }
}