use std::collections::HashMap;
use std::fmt;
#[derive(Debug)]
pub struct PaliPermu{
    palindrome: String,
    hash: HashMap<char, i32>,
    matching_letters: Vec<char>,
    unmatching_letters: Vec<char>,
    left_palindrome: Vec<char>,
    center_palindrome: Vec<String>,
    swaps: Vec<usize>,
    i: usize,
}

impl fmt::Display for PaliPermu{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut x = String::from(format!("Struct PaliPermu:\n"));
        x += &format!("palindrome:{}\n", &self.palindrome);
        x += &format!("hash:{:?}\n", &self.hash);
        x += &format!("matching_letters: {:?}\n", &self.matching_letters);
        x += &format!("unmatching_letters: {:?}\n", &self.unmatching_letters);
        x += &format!("left_palindrome: {:?}\n", &self.left_palindrome);
        x += &format!("center_palindrome: {:?}\n", &self.center_palindrome);
        //x += &format!("{:?}");
        write!(f, "{}\n", x)
    }
}

impl Iterator for PaliPermu {
    //use Heap's algorithm, adapted from https://rosettacode.org/wiki/Permutations#Iterative
    type Item = Vec<String>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.i > 0 {
            loop {
                if self.i >= self.swaps.len() { return None; }
                if self.swaps[self.i] < self.i { break; }
                self.swaps[self.i] = 0;
                self.i += 1;
            }
            self.left_palindrome.swap(self.i, (self.i & 1) * self.swaps[self.i]);
            self.swaps[self.i] += 1;
        }
        self.i = 1;
        let mut v: Vec<String> = Vec::new();
        let right_palindrome = self.left_palindrome.iter().rev().collect::<String>();
        if self.center_palindrome.len() > 0 {
            for s in &self.center_palindrome {
                v.push(format!("{}{}{}",
                self.left_palindrome.iter().collect::<String>(),
                s.to_string(),
                right_palindrome));
            }
        }
        else {
            v.push(format!("{}{}", self.left_palindrome.iter().collect::<String>(), right_palindrome));
        }

        Some(v)
    }
}

impl PaliPermu {
    pub fn new(palindrome: String) -> Result < PaliPermu, String >{
        let mut hash = HashMap::new();
        let matching_letters;//chars that have a corresponding char in the palindrome
        let mut unmatching_letters;
        let mut left_palindrome;
        let mut center_palindrome = vec![];

        let palindrome = palindrome.to_ascii_lowercase();

        for c in palindrome.to_lowercase().chars() {
            let count = hash.entry(c).or_insert(0); //add char if not present and set to 0. return pointer to value
            *count += 1;
        }

        //The palindrome could have 4 'a' in it -> 'aabaa' or 5 -> 'aaaaa'
        let x = hash.keys().filter(|y| *hash.get(&y).unwrap() >= 3 && *hash.get(&y).unwrap() % 2 != 0).cloned().collect::<Vec<char>>();
        //pull out the odd value and store it away on its own, decrement the odd value
        for c in x {
            hash.insert(c.to_ascii_uppercase(), 1);//Differentiate the odd valued letters as Uppercase
            hash.insert(c, hash.get(&c).unwrap() - 1);
        }

        matching_letters = hash.keys().filter(|x| hash.get(x).unwrap() % 2 == 0).cloned().collect::<Vec<char>>();
        unmatching_letters = hash.keys().filter(|x| !matching_letters.contains(x)).cloned().collect::<Vec<char>>();
        unmatching_letters = unmatching_letters.iter().map(|x| x.to_ascii_lowercase()).collect();

        left_palindrome = palindrome.chars().collect::<Vec<char>>();

        left_palindrome.sort();

        for c in &unmatching_letters{
            left_palindrome.remove(left_palindrome.iter().position(|x| x==c).unwrap());
        }

        left_palindrome = left_palindrome.iter().step_by(2).map(|x| *x).collect::<Vec<char>>();

        center_palindrome.push(unmatching_letters.iter().collect());
        center_palindrome.push(unmatching_letters.iter().rev().collect());
        center_palindrome.dedup();
        let z = left_palindrome.len(); //yikes

        return match PaliPermu::is_a_palindrome(&palindrome, &hash){
            Ok(()) => Ok(PaliPermu {
                palindrome,
                hash,
                matching_letters,
                unmatching_letters,
                left_palindrome,
                center_palindrome,
                swaps: vec![0; z],
                i: 0 }),
            Err(error) => Err(error),
        }
    }

    fn is_a_palindrome(palindrome: &String, hash: &HashMap<char, i32>) -> Result <(), String>{
        if hash.is_empty(){// || hash.len() == 1{
            return Err(format!("Empty string passed in."))
        }

        if hash.len() < 2 {
            return Err(format!("'{}' is too short to be a palindrome.", palindrome))
        }
        match hash.values().filter(|y| **y == 1).count()  {
            //0 and 1 single entries are ok. 'abccba' or 'abcxcba' / 'abc cba'
            0 | 1 => {},
            //2 single entries are ok if one of them is ' '
            2 => if !hash.contains_key(&' ') {
                return Err(format!("'{}' is not a palindrome, it has 2 or more unmatched letters", palindrome))
            },
            //3 or more single entries are not a palindrome
            _ => return Err(format!("'{}' is not a palindrome, it has 3 or more unmatched letters", palindrome))
        }
        Ok(())
    }
}