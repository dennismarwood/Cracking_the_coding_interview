//mod lib;
//use _1_4;
#![allow(non_snake_case)]

mod tests {
    fn get_purmus(pali_permu: _1_4::PaliPermu) -> String {
        let mut res = String::new();
        for (i, x) in pali_permu.enumerate() {
            let permus = ||{
                let mut z = x.iter().map(|x| x.clone()).collect::<String>();
                if x.len() == 2 { z.insert_str(z.len()/2, "  -AND-  ");}
                z
            };
            res.push_str(&format!("{} - {}\n", i, permus()).to_string());
        }
        res
    }

    #[test]
    fn aabbcc() {
        let pali_permu = _1_4::PaliPermu::new("aabbcc".to_string()).unwrap();
        let expected_value = "\
0 - abccba
1 - baccab
2 - cabbac
3 - acbbca
4 - bcaacb
5 - cbaabc
";
        assert_eq!(expected_value, get_purmus(pali_permu));
    }

    #[test]
    fn zaZZzaz() {
        let pali_permu = _1_4::PaliPermu::new("zaZZzaz".to_string()).unwrap();
        let expected_value = "\
0 - azzzzza
1 - zazzzaz
2 - zazzzaz
3 - azzzzza
4 - zzazazz
5 - zzazazz
";
        assert_eq!(expected_value, get_purmus(pali_permu));
    }

    #[test]
    fn zaZZzaz_with_space() {
        let pali_permu = get_purmus(_1_4::PaliPermu::new("z aZZzaz".to_string()).unwrap());
        let expected_value = "azz zzza<>azzz zza<>zaz zzaz<>zazz zaz<>zaz zzaz<>zazz zaz<>azz zzza<>azzz zza<>zza zazz<>zzaz azz<>zza zazz<>zzaz azz";
        for chunk in expected_value.split("<>") {
            assert!(pali_permu.contains(chunk), "\n{} \nnot found in \n{}", chunk, pali_permu);
        }
    }

    #[test]
    fn empty_string() {
        let z:Result<_, String> =_1_4::PaliPermu::new("".to_string());
        assert_eq!("Empty string passed in.", z.unwrap_err())
    }

    #[test]
    fn too_short(){
        let z:Result<_, String> =_1_4::PaliPermu::new("a".to_string());
        assert_eq!("'a' is too short to be a palindrome.", z.unwrap_err())
    }

    #[test]
    fn two_or_more_unmatched(){
        let z:Result<_, String> =_1_4::PaliPermu::new("aaAZZzzz".to_string());
        assert_eq!("'aaazzzzz' is not a palindrome, it has 2 or more unmatched letters", z.unwrap_err())
    }

    #[test]
    fn three_or_more_unmatched(){
        let z:Result<_, String> =_1_4::PaliPermu::new("aaA ZZzzzqqq".to_string());
        assert_eq!("'aaa zzzzzqqq' is not a palindrome, it has 3 or more unmatched letters", z.unwrap_err())
    }
}