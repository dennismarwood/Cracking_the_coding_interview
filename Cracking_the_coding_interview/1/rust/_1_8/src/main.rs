/*
Zero Matrix: Write an algorithm such that if an element in an MxN matrix is 0, its entire row and column are set to 0. 
*/
use std::collections::HashSet;
use yansi::Paint;

fn main() {
    const MATRIX_SIZE: usize = 7;
    let mut matrix: [[usize;MATRIX_SIZE]; 8] = [[1; MATRIX_SIZE], [2; MATRIX_SIZE], [3; MATRIX_SIZE], [4; MATRIX_SIZE], [5; MATRIX_SIZE], [6; MATRIX_SIZE ], [7; MATRIX_SIZE ], [8; MATRIX_SIZE ]];
    println!("{}", matrix.len());
    matrix[1][0] = 0;
    matrix[1][3] = 0;
    matrix[4][2] = 0;
    print_matrix(&matrix);


    let mut column_targets = HashSet::new();
    let mut row_targets = HashSet::new();

    for (ix, x) in matrix.iter().enumerate() {
        for (iy, y) in x.iter().enumerate() {
            if *y == 0{
                row_targets.insert(ix); //hashset keeps unique only
                column_targets.insert(iy);
            }
        }
    }

    for x in row_targets.iter() {
        matrix[*x] = [0; MATRIX_SIZE];
    }

    for x in column_targets.iter() {
        for y in 0..8 {
            matrix[y][*x] = 0;
        }
    }

    print_matrix(&matrix);

    fn print_matrix(m: &[[usize;MATRIX_SIZE]; 8]) -> String{
        let mut result = String::from("/n");
        println!("");
        for x in m.iter() {
            for y in x.iter() {
                result += &format!("[{}]", y);
                if *y == 0 {
                    print!("[{}]", Paint::green(y));
                } else {
                    print!("[{}]", Paint::blue(y));
                }
            }
            println!("");
            result += " ";
        }
        result
    }
}