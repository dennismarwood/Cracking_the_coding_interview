/*
Rotate Matrix: Given an image represented by an NxN matrix, where each pixel in the image is 4 bytes, write a method to rotate the image by 90 degrees. Can you do this in place?
*/
fn main() {
    const matrix_size: usize = 7;
    //let mut matrix: [[usize;matrix_size];matrix_size] = [[1; matrix_size], [2; matrix_size], [3; matrix_size], [4; matrix_size]];
    //let mut matrix: [[usize;matrix_size];matrix_size] = [[1; matrix_size], [2; matrix_size], [3; matrix_size], [4; matrix_size], [5; matrix_size]];
    //let mut matrix: [[usize;matrix_size];matrix_size] = [[1; matrix_size], [2; matrix_size], [3; matrix_size], [4; matrix_size], [5; matrix_size], [6; matrix_size ]];
    let mut matrix: [[usize;matrix_size];matrix_size] = [[1; matrix_size], [2; matrix_size], [3; matrix_size], [4; matrix_size], [5; matrix_size], [6; matrix_size ], [7; matrix_size ]];
    

    fn print_matrix(m:& [[usize;matrix_size];matrix_size]) -> String{
        let mut result = String::from("/n");
        println!("");
        for x in m.iter() {
            for y in x.iter() {
                result += &format!("[{}]", y);
                print!("[{}]", y);
            }
            println!("");
            result += " ";
        }
        result
    }


    fn rotate_matrix(matrix: [[usize;matrix_size];matrix_size]) -> [[usize;matrix_size];matrix_size] {
        for j in 0..matrix_size / 2 {// for each "layer" of a matrix
            println!("processing layer {}", j);
            for i in j..matrix_size -j -1 { //for each top element in the "layer". Don't move the last column because its new value will already have been shuffled in.
                print_matrix(&matrix);
                let current_location: (usize, usize) = (j,i);
                println!("Move {:?}", current_location);
                let mut trailing_temp: usize = matrix[current_location.0][current_location.1];
                let mut temp: usize;
                for new_location in NextLocation::new(current_location, matrix_size) { //Shift elements on the layer around
                    //println!("new_location {:?}", new_location);
                    temp = matrix[new_location.0][new_location.1]; //Store value in the next locations box
                    matrix[new_location.0][new_location.1] = trailing_temp; //Set value at next location to current location
                    trailing_temp = temp;
                }
            }
        }
    }

    print_matrix(&matrix);

    struct NextLocation {
        location: (usize, usize),
        matrix_size: usize,
        swap_count: usize,
    }

    impl NextLocation {
        fn new(location: (usize, usize), m: usize) -> NextLocation {
            NextLocation { location: location, matrix_size: m, swap_count: 0 }
        }
    }

    impl Iterator for NextLocation {
        type Item = (usize, usize);

        fn next(&mut self) -> Option<Self::Item> {
            self.swap_count +=1;
            self.location = (self.location.1, (matrix_size - 1 ) - self.location.0);
            if self.swap_count != 5 {
                println!("returning: {:?}", self.location);
                Some(self.location)
            }
            else { None }
        }
    }
}
