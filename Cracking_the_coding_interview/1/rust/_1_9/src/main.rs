/*
String Rotation: Assume you have a method isSubstring which checks if one word is a substring of another. Given two strings, s1 and s2, write code to check if 
s2 is a rotation of s1 using only one call to isSubstring (e.g, "waterbottle" is a rotation of "erbottlewat").
*/

//This problem is not about rotating strings, but rather about realizing that rotations can be identified by concat and checking for a substring.
fn main() {
    let s1 = "erbottldewat";
    let s2 = "waterbottle".to_string();

    print! ("\n'{}' is", &s1);
    if !([s1, s1].concat()).contains(&s2) {
        print! (" NOT");
    }
    println!(" a rotation of '{}'\n", s2);

}
