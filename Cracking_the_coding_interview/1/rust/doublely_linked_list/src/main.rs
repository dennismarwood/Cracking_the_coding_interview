
//Credit due to the author of https://play.rust-lang.org/?gist=c3db81ec94bf231b721ef483f58deb35
//I am using this a foundation for learning rust's different pointers.
use yansi::Paint;

use std::cell::RefCell;
//Single owner reference. Borrowing rules are enforced at run time and errors can cause panic.
//Let programmer be sure that they are following borrowing rules, not the compiler. The compiler can't see all valid code and will panic. This lets you bypass.
//Valid only for single threaded scenario.
//Allows immutable or mutable borrows at runtime.

use std::rc::{Rc, Weak};
//RC (Rc::clone)
// Allows multiple ownwers of an object. As each owner's scope ends and the destructor is run the count on the RC is reduced. When it is zero it is cleaned up.
// References are immutable only.
//WEAK (Rc::downgrade)
//Does not increase the Rc count (the strong_count).
//Rc will be cleaned up if strong_count is zero even if weak cound is >0.
//Do not express ownerhip relationship.
//You must check that the referenced value still exists. Do this with upgrade and get an option

use std::fmt::Display;
// It uses an Rc (Reference Counted) pointer to give ownership of the right node
// to the current node. And a Weak (weak Reference Counted) pointer to reference
// the left node without owning it.
//
// It uses RefCell for interior mutability. It allows mutation through
// shared references.
struct Node<T> {
    pub data: T,
    pub left: Option<Weak<RefCell<Node<T>>>>,
    //A weak pointer to a RC pointer to the node. This node can mutate the left node. But it will see Option(None) if left node is deleted when it does upgrade.
    //We are able to mutate an object, via RC borrow, but only after we are forced to ensure that it is still there. Pretty nice. See the bottom of https://doc.rust-lang.org/book/ch15-06-reference-cycles.html?highlight=weak#visualizing-changes-to-strong_count-and-weak_count

    pub right: Option<Rc<RefCell<Node<T>>>>,
    //The references with ownership are moving right down the chain towards the tail.
}

impl<T> Node<T> {
    // Constructs a node with some `data` initializing left and right to null.
    pub fn new(data: T) -> Self {
        Self { data, left: None, right: None }
    }

    pub fn append(focus_node: &mut Rc<RefCell<Node<T>>>, data: T) -> Option<Rc<RefCell<Node<T>>>> {
        //Rust is doing some automatic dereferencing here.
        //https://doc.rust-lang.org/book/ch15-05-interior-mutability.html#having-multiple-owners-of-mutable-data-by-combining-rct-and-refcellt
        //https://doc.rust-lang.org/book/ch05-03-method-syntax.html#wheres-the---operator
        if focus_node.borrow().right.is_none() { //immutable borrow, but drops out of scope before block?
            // If the current node is the last one, create a new node,
            // set its left pointer to the current node, and store it as
            // the node after the current one. 
            let mut new_node = Node::new(data); //new node here 
            new_node.left = Some(Rc::downgrade(&focus_node)); //Here is the weak ref. Set new node left pointer. The '&' gaining ref is automatically finding the RefCell pointer of the node, not creating a new ref.
            let rc = Rc::new(RefCell::new(new_node));
            focus_node.borrow_mut().right = Some(rc.clone());//borrow right again, this time mutable
            Some(rc)
        } else {
            // Not the last node, just continue traversing the list:
            if let Some( ref mut next_right) = focus_node.borrow_mut().right { //Sets the variable "next_right" See https://doc.rust-lang.org/stable/rust-by-example/scope/borrow/ref.html
                //for info on what ref does. It is used in pattern matching (for the "if let" here) to take references of the object. I think this stops some of rust's automatic dereferencing.
                Self::append(next_right, data) //now focus on the node to the right. Does next_right carry the mutable borrow along with it? 
            } else { None }
        }
    }

    pub fn insert_to_right_of_node(focus_node: &mut Rc<RefCell<Node<T>>>, data: T) -> Option<Rc<RefCell<Node<T>>>> {
        //New node
        let mut new_node = Node::new(data);

        //Set new node left and right
        new_node.left = Some(Rc::downgrade(&focus_node));
        new_node.right = focus_node.borrow().right.clone();

        //Set focus_node right value to the new node
        let rc = Rc::new(RefCell::new(new_node));
        focus_node.borrow_mut().right = Some(rc.clone());

        //If there was a node to the right of focus_node, then set -that- nodes left to the new_node
        if let Some(old_right) = rc.borrow().right.clone(){
            old_right.borrow_mut().left = Some(Rc::downgrade(&rc));
        }
        Some(rc)
    }
}

// The doubly-linked list with pointers to the head and tail nodes in the list.
struct List<T> {
    head: Option<Rc<RefCell<Node<T>>>>,
    tail: Option<Rc<RefCell<Node<T>>>>,
}

impl<T> List<T> {
    // Constructs an empty list.
    pub fn new() -> Self {
        Self { head: None, tail: None }
    }
    // Appends a new node to the list, handling the case where the list is empty.
    pub fn grow(&mut self, data: T) {
        //https://doc.rust-lang.org/book/ch06-03-if-let.html
        //Says if self.head is not Option(None) (it is a ref mut type in this case) then store that value in "right" and execute the block
        //In other words, if head is already set to a node then update list tail to a new node that will be inserted at end of list.
        if let Some(ref mut right) = self.head {
            self.tail = Node::append(right, data);
        } else {
            //Otherwise list must be empty, construct RC->RefCell->new node and set it as both head and tail.
            let f = Rc::new(RefCell::new(Node::new(data))); //Strong count is 1
            self.head = Some(f.clone());//New pointer to node created above. Strong count is now 2
            //println!("SC for f {}", Rc::strong_count(&f));
            self.tail = Some(f);//A move of the RC here. Strong count remains at 2.
            //println!("SC for f {}", Rc::strong_count( &self.tail.as_ref().unwrap() ));
        }
    }

    pub fn insert(&mut self, data: T, target_index: usize) {
        if self.head.is_none() {
            self.grow(data);
            return
        }
        let mut focus_index = 0;
        //let mut focus_node = self.head.unwrap().borrow();
        let mut focus_node = self.head.clone();
        //Some( ref mut next_right) = focus_node.borrow_mut().right
        //while focus_node.unwrap().borrow().right.is_some() && focus_index != target_index {
        while let Some(next_right) = focus_node.clone().unwrap().borrow().right.clone() {

            if focus_index == target_index {
                break;
            }
            focus_index += 1;
            focus_node = Some(next_right);
        }
        //if adding new head (target_index was 0)
        if focus_node.clone().unwrap().borrow().left.is_none() {
            self.head = Node::insert_to_right_of_node(&mut focus_node.unwrap(), data);
            return
        }

        //if adding new tail (target_index >= len of list)
        if focus_node.clone().unwrap().borrow().right.is_none() {
            self.tail = Node::insert_to_right_of_node(&mut focus_node.unwrap(), data);
            return
        }
        //if adding to middle of list
        Node::insert_to_right_of_node(&mut focus_node.unwrap(), data);
    }
}

// Pretty-printing
impl<T: Display> Display for List<T> {
    fn fmt(&self, w: &mut std::fmt::Formatter) -> std::result::Result<(), std::fmt::Error> {
        write!(w, "{}", Paint::green("~<:|"))?;
        let mut node = self.head.clone();
        while let Some(n) = node {
            //write!(w, "{}-{}", n.borrow().data, Rc::strong_count(&n) -1 )?; //Note that the clone above increments the Strong Count
            write!(w, " {} ", Paint::blue(&n.borrow().data))?; //Note that the clone above increments the Strong Count
            node = n.borrow().right.clone();
            if node.is_some() {
                write!(w, "{}", Paint::green("|"))?;
            }
        }
        write!(w, "{}", Paint::green("|>"))
    }
}

fn main() {
    let mut snake = List::new();
    //println!("{}", snake);
    snake.insert(0, 100);
    for i in 1..4{
        snake.grow(i);
        //println!("{}", snake);
    }
    snake.insert(9, 0);
    snake.insert(99, 2);
    snake.insert(999, 10);
    println!("{}", snake);
    //println!("head strong count {}", Rc::strong_count(& snake.head.clone().unwrap() )); //This clone will increment the Strong count tainting the results.
    println!("SC for head {}", Rc::strong_count( &snake.head.as_ref().unwrap() ));
    println!("SC for tail {}", Rc::strong_count( &snake.tail.as_ref().unwrap() ));
    println!("head: {}", snake.head.clone().unwrap().borrow().data );
    println!("tail: {}", snake.tail.clone().unwrap().borrow().data );

}
