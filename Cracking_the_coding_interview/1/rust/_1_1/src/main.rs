/*
Is Unique: Implement an algorithm to determine if a string has all unique characters. What if you cannot use data structures?
*/
use std::io;

fn main() {
    let mut guess: String = String::new();
    io::stdin().read_line(&mut guess)
    .expect("fail");

    'outer: for (i, target) in guess.chars().enumerate() {
        //println!("Searching for: {}", target);
        for n in i+1..guess.chars().count(){
            match guess.chars().nth(n) {
                None => break,
                Some(c) => if target == c {
                    println!("Match found for: {} at {} and {}", target, i, n);
                    break 'outer;
                }
            };
        }
    }
}
