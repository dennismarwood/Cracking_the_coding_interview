/*
Given two strings, write a method to decide if one is a permutation of the other.
*/
use std::io;
use std::collections::HashMap;

fn main() {
    let mut guess1: String = String::new();
    let mut guess2: String = String::new();

    io::stdin().read_line(&mut guess1)
    .expect("fail guess1");
    guess1 = guess1.trim().to_string();

    io::stdin().read_line(&mut guess2)
    .expect("fail guess2");
    guess2 = guess2.trim().to_string();

    let mut map: HashMap<char, i32> = HashMap::new();

    for g1 in guess1.chars(){
        let count = map.entry(g1).or_insert(0);
        *count += 1;
    }

    for g2 in guess2.chars(){
        let count = map.entry(g2).or_insert(0);
        *count -= 1;
        if map[&g2] == 0 {
            map.remove(&g2);
        }
    }

    if map.is_empty(){
        println!("'{}' and '{}' are permutations", guess1, guess2);
    }
    else {
        println!("'{}' and '{}' are NOT permuations", guess1, guess2);
    }

}
