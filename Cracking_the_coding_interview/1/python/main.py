'''1'''
#import argparse
import hashlib
import random
import string


ENTRIES = set([''.join(random.sample(string.ascii_lowercase, 5)) for _ in range(10000)])
DEL_ENTRIES = [random.choice(list(ENTRIES)) for _ in range(1000)]

#parser = argparse.ArgumentParser()
#parser.add_argument("hash", help="A string to perform a hash against.")
#args = parser.parse_args()

#print(args.hash)
#print('Will hash: {}'.format(args.hash))

#my_hash = hash(args.hash)

#y = hashlib.sha224((args.hash).encode('utf-8')).hexdigest()

#print("Created hash: {}".format(my_hash))
#my_hash = str(my_hash)[-2:]
#print("Will use: {}".format(my_hash))

#print(hash(args.hash))

class node(): #pylint: disable=R0903
    '''Node in a doublelinked list. User must give a key value.'''

    def __init__(self, key, value=None):
        self.key = key
        self.value = value
        self.prev = None
        self.next = None

    def __repr__(self):
        return "key: {0} value: {1} Prev: {2} Next: {3}".format(self.key,
                                                                self.value,
                                                                self.prev.key if self.prev else " ",
                                                                self.next.key if self.next else " ")

class doublyLinkedList(): #pylint: disable=R0903
    '''Holds Nodes. Overloads + and - and has get method.'''

    def __init__(self, sentinel=None):
        self.sentinel = sentinel
        self.length = 0

    def get(self, key): #pylint: disable=C0111
        cursor = self.sentinel
        while cursor:
            if cursor.key == key:
                return cursor.value
            cursor = cursor.next
        return None

    def __len__(self):
        return self.length

    def __add__(self, new_node):
        assert isinstance(new_node, node)
        self.length += 1

        #list is empty
        if not self.sentinel:
            self.sentinel = new_node
            return

        cursor = self.sentinel
        while cursor:
            #User updating key's value
            if cursor.key == new_node.key:
                cursor.value = new_node.value
                self.length -= 1
                return

            #key is smaller than any in the list
            if cursor.key > new_node.key:
                new_node.next = cursor
                cursor.prev = new_node
                self.sentinel = new_node
                return

            if cursor.next:

                #Move forward if the next key is smaller than the new
                if cursor.next.key <= new_node.key:
                    cursor = cursor.next
                    continue

                #Adding to middle of list
                cursor.next.prev = new_node
                new_node.prev = cursor
                new_node.next = cursor.next
                cursor.next = new_node
                return

            #Key is bigger than any in the list
            cursor.next = new_node
            new_node.prev = cursor
            return

    def __sub__(self, target_key):
        assert isinstance(target_key, str)
        #identify node stall
        self.length -= 1
        stall = hashlib.sha224(target_key.encode('utf-8')).hexdigest()
        stall = int(str(int(stall, 16))[-2:])

        cursor = self.sentinel

        while cursor:
            if cursor.key < target_key:
                cursor = cursor.next
                continue

            if cursor.key == target_key:
                if cursor.next:
                    cursor.next.prev = cursor.prev
                if cursor.prev:
                    cursor.prev.next = cursor.next
                #May need to update the sentinel
                if cursor == self.sentinel:
                    self.sentinel = cursor.next
                cursor = None
                return True

            cursor = cursor.next
        #Key was not present
        self.length += 1
        return None


    def __repr__(self):
        result = ""
        cursor = self.sentinel
        while cursor:
            result += str(cursor) + "\n"
            cursor = cursor.next
        return result

class dlls(): #pylint: disable=R0903
    '''dlls'''
    stalls = [doublyLinkedList() for i in range(100)]

    def __getitem__(self, key):
        i = hashlib.sha224(key.encode('utf-8')).hexdigest()
        i = int(str(int(i, 16))[-2:])
        return self.stalls[i].get(key)

    def __setitem__(self, key, value):
        i = hashlib.sha224(key.encode('utf-8')).hexdigest()
        i = int(str(int(i, 16))[-2:])
        self.stalls[i] + node(key, value) #pylint: disable=W0106

    def __add__(self, new_node):
        '''You can use + if you want, but you have to pass a node.'''
        i = hashlib.sha224(new_node.key.encode('utf-8')).hexdigest()
        i = int(str(int(i, 16))[-2:])
        self.stalls[i] + new_node #pylint: disable=W0104

    def __sub__(self, target_key):
        i = hashlib.sha224(target_key.encode('utf-8')).hexdigest()
        i = int(str(int(i, 16))[-2:])
        print("delete: {0} from stall: {1}".format(target_key, i))
        self.stalls[i] - target_key #pylint: disable=W0104

def print_dlls():
    '''print all the dll'''
    for i, d in enumerate(dlls.stalls):
        if str(d) != "":
            print("Stall:{0}\n{1}".format(str(i), str(d)))
    print("How many Nodes per dll? (Collision vs Memory)")
    for i, d in enumerate(dlls.stalls):
        print("{0} - {1}:".format(str(i), str(len(d))))


if __name__ == "__main__":
    # execute only if run as a script
    my_dict = dlls()

    for word in ENTRIES:
        my_dict[word] = word[-1::-1]

    print_dlls()

    for word in DEL_ENTRIES: #could have made it del dlls[word] but meh. User can update key to None
        my_dict - word #pylint: disable=W0104

    print_dlls()

    for x in range(5):
        r = random.choice(list(ENTRIES))
        print("What is the value for the key {0}? It is {1}.".format(r, my_dict[r]))

    '''
    dlls[0] + node("A", "AA")
    dlls[0] + node("B", "BB")
    dlls[0] + node("C", "CC")
    dlls[0] + node("D", "DD")
    print(dlls[0])
    dlls[0] - "B"
    print(dlls[0])
    dlls[0] + node("B", "BB")
    print(dlls[0])
    dlls[0] - "A"
    dlls[0] - "C"
    dlls[0] - "D"
    dlls[0] - "B"
    dlls[0] + node("B", "BB")
    print(dlls[0])'''
