import unittest
import sys
from ..main import node, doublyLinkedList
#from .. import python

#import sys
#import os

#sys.path.append(os.path.abspath('../main'))
#from main import *

#pylint: disable=C0111

class doublyLinkedListTestCase(unittest.TestCase):
    '''Test the inserting and removal of nodes'''
    def setUp(self):
        self.dll = doublyLinkedList()
        self.a_node = node("A", "AA")
        self.b_node = node("B", "BB")
        self.c_node = node("C", "CC")
        self.d_node = node("D", "DD")
        self.e_node = node("E", "EE")
        self.f_node = node("F", "FF")
        self.g_node = node("G", "GG")
        self.h_node = node("H", "HH")

    def tearDown(self):
        del self.dll
        del self.a_node
        del self.b_node
        del self.c_node
        del self.d_node
        del self.e_node
        del self.f_node
        del self.g_node
        del self.h_node

    def test_add_to_empty(self):
        self.dll + self.a_node #pylint: disable=W0104
        self.assertEqual(len(self.dll), 1)
