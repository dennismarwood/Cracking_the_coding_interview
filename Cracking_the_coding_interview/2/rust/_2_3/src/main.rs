//Delete middle node: Implement an algorigthm to delete a node in the 
//middle (i.e., any node but the first and last node, not necessarily
//the exact middle) of a singly linked list, given only access to that
//node.
//EXAMPLE
//Input: the node c from the linked list a->b->c->d->e->f
//Result: nothing is returned, but the new linked list looks like
//a->b->d->e->f

use dll::my_lib::list::List;
use dll::my_lib::node::Node;
use rand::Rng;


pub trait ListExt<T> {
    fn remove_middle_node(&mut self);
}

impl<T: std::fmt::Debug + std::hash::Hash + std::cmp::Eq + std::cmp::PartialEq> ListExt<T> for List<T> {
    fn remove_middle_node(&mut self) {
        if self.size < 3 { 
            println!("List is not long enough to remove a 'middle' node.");
            return
        }    
        let mut f = self.head.clone();
        for _ in 0..rand::thread_rng().gen_range(2, self.size)-1 {
            f = f.unwrap().borrow().right.clone();
        }
        Node::remove_node(f.unwrap());
        self.size -= 1;
    }
}

fn main() {
    let mut snake = List::new();
    for i in 1..11{
        snake.push(i);
    }
    println!("{}", snake);
    snake.remove_middle_node();
    println!("{}", snake);
}
