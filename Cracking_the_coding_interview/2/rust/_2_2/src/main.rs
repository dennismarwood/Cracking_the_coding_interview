//Return Kth to Last: Implement an algorithm to find the kth to last element of a singly linked list.

use dll::my_lib::list::List;
use dll::my_lib::node::NodeRef;

pub trait ListExt<T> {
    fn kth_to_last(&self, k: isize);
}

//coerce T.data into string in the same way "println!" does and return that as a string instead?
impl<T: std::fmt::Debug> ListExt<T> for List<T> {
    //have trailing pointer k spaces behind lead pointer
    //when lead pointer hits end of list, return trailing pointer
    fn kth_to_last(&self, k: isize){
        let mut trailing: Option<NodeRef<T>> = None;
        let mut focus = self.head.clone();
        let mut count = 0;
        while let Some(f) = focus {
            if count == k - 1 {
                trailing = self.head.clone();
            }
            focus = f.borrow().right.clone();
            if focus.is_some() && trailing.is_some() {
                trailing = trailing.unwrap().borrow().right.clone();
            }
            count += 1;
        }
        //trailing.clone()
        match trailing {
            None => println!("snake does not have an element {} from last", k),
            Some(r) => println!("{} to last of snake is {:?}", k, r.borrow().data),
        }
    }
}


fn main() {
    let mut snake = List::new();

    for i in 1..5{
        snake.grow(i);
    }
    println!("{}", snake);
    snake.insert(8, 0);
    snake.insert(8, 0);
    snake.insert(8, 0);
    snake.insert(8, 8);
    snake.insert(8, 88);
    println!("{}", snake);
    snake.de_dup_with_buffer();
    println!("Deduped with buffer, now print\n{}", snake);
    snake.kth_to_last(0);
    snake.insert(4, 1);
    snake.de_dup_with_buffer();
    println!("Deduped with buffer, now print\n{}", snake);
}
