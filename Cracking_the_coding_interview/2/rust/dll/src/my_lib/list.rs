use crate::my_lib::node::{Node, NodeKey};

use yansi::Paint;
use std::collections::HashSet;
use std::cell::RefCell;
use std::rc::Rc;
use std::fmt::Display;

pub type NodeRef<T> = Rc<RefCell<Node<T>>>;

// The doubly-linked list with pointers to the head and tail nodes in the list.
pub struct List<T> {
    pub head: Option<NodeRef<T>>,
    pub tail: Option<NodeRef<T>>,
    pub size: i32,
}

impl<T: std::fmt::Debug + std::hash::Hash + std::cmp::Eq > List<T> {
    // Constructs an empty list.
    pub fn new() -> Self {
        Self { head: None, tail: None, size: 0}
    }
    // Appends a new node to the list, handling the case where the list is empty.
    pub fn grow(&mut self, data: T) {
        self.size += 1;
        if let Some(ref mut right) = self.head {
            self.tail = Node::append(right, data);
        } else {
            let f = Rc::new(RefCell::new(Node::new(data))); //Strong count is 1
            self.head = Some(f.clone());//New pointer to node created above. Strong count is now 2
            self.tail = Some(f);//Move the existing pointer here. Strong count remains at 2
            //println!("SC {}", self.head.as_ref().map(|t| Rc::strong_count(&t)).unwrap());
        }
    }

    pub fn de_dup_with_buffer(&mut self) {
        let mut hashset = HashSet::new();
        let mut focus = self.head.clone();
        while let Some(f) = focus {
            let next = f.borrow().right.clone();
            if !hashset.insert(NodeKey(f.clone())) {
                //Check if we are about to try and remove node tail has strong count on. remove_node(node) will fail if target has >1 strong count on it.
                if self.tail.as_ref().map(|t| Rc::ptr_eq(&t, &f)).unwrap_or(false) {
                    self.tail = f.borrow().left.as_ref().and_then(|l| l.upgrade());
                }
                println!("Removed node with value: {:?}", Node::remove_node(f).unwrap());
                self.size -= 1;
            }
            focus = next;
        }
    }

    pub fn de_dup_without_buffer(&mut self) {
        let mut focus = self.head.clone();
        while let Some(f) = focus {
            
            let mut runner = f.borrow().right.clone();
            while let Some(r) = runner {
                let next = r.borrow().right.clone();
                if f == r {
                    if self.tail.as_ref().map(|t| Rc::ptr_eq(&t, &r)).unwrap_or(false) {
                        self.tail = r.borrow().left.as_ref().and_then(|l| l.upgrade());
                    }
                    println!("Removed node with value: {:?}", Node::remove_node(r).unwrap());
                    self.size -= 1;
                }
                runner = next;
            }

            let next = f.borrow().right.clone();
            focus = next;
        }
    }

    pub fn insert(&mut self, data: T, target_index: usize) {
        if self.head.is_none() {
            self.grow(data);
            return
        }
        let mut focus_index = 0;
        let mut focus_node = self.head.clone();

        while let Some(next_right) = focus_node.clone().unwrap().borrow().right.clone() {

            if focus_index == target_index {
                break;
            }

            focus_node = Some(next_right);
            focus_index += 1;
        }

        //if adding new tail (target_index >= len of list)
        if focus_node.clone().unwrap().borrow().right.is_none() {
            self.tail = Node::insert_to_right_of_node(&mut focus_node.unwrap(), data);
            self.size += 1;
            return
        }
        //if adding to middle of list
        Node::insert_to_right_of_node(&mut focus_node.unwrap(), data);
        self.size += 1;
    }

    pub fn push(&mut self, data: T) {
        if self.head.is_none() {
            self.grow(data);
            return
        }
        self.size += 1;
        let new_node = Rc::new(RefCell::new(Node::new(data)));
        self.head.clone().unwrap().borrow_mut().left = Some(Rc::downgrade(&new_node));
        new_node.borrow_mut().right = self.head.clone();
        self.head = Some(new_node);
    }

    //List should just be adjusting self.head and self.tail
    pub fn pop(&mut self) -> Option<T> {
        self.size -= 1;
        //println!("Head Left: {:?} Head Right: {:?}", self.head.clone().unwrap().borrow().left, self.head.clone().unwrap().borrow().right);
        //let node = self.head.unwrap().get_mut(); GET JUST NODE HERE
        //Get the head as a ref so you don't move it; unwrap the reference; 
        //borrow the ref cell; clone whatever is there so that it can 
        //be passed by value (for into); convert

        //let head = self.head.take().unwrap();
        //Some(Rc::try_unwrap(head).ok().unwrap().into_inner().data)

        let head = self.head.take()?; //Take here instead of unwrap because unwrap
        //will panic if it is None. 

        // ? - Applies to results. Instead of writing out a match statement and returning
        // either OK or Err, this is shorthand. It unwraps and OK and returns and Err.
        // It also works with Option<T>. It returns either Some(T) or None. 

        // take - Takes the value out of the option, leaving a None in its place.
        // Since head is an option first this "take" will move the Rc into "head".
        // The '?' will then unwrap it and return an error if it is None.

        //if tail points to same node as head, then set tail to None.
        if self.tail.as_ref().map(|t| Rc::ptr_eq(&t, &head)).unwrap_or(false) {
            // as_ref - convert Option<RC> to Option<&RC> . It is how we 'borrow' the RC
            // Better than Some(&(self.tail.clone().unwrap()))
            // map - consumes 't' meaning that it will not live past the scope
            // of the map call. It takes 't' by value so we need to pass in 't'
            // by ref here. Otherwise Rust will bark at us for trying to move tail.
            // ptr_eq - Returns true if both pointers point to the same Node.
            // unwrap_or - If the value passed it is Ok (as opposed to Err) then return
            // the value. If Err then return the provided default (false in our case).
            // Now if we try to to compare on a None and get an error, we can just return false.

            self.tail = None;
        }

        let (left, right) = {
            let mut head = head.borrow_mut();
            (head.left.take(), head.right.take())
        };

        if let Some(left) = left.and_then(|w| w.upgrade()) {
            left.borrow_mut().right = right;
        } else {
            self.head = right;
        }
        Node::remove_node(head)
        
        
        //Some(Rc::try_unwrap(head).ok()?.into_inner().data)
        // try_unwrap - If the Rc has exactly one strong reference then return
        // the inner value. Otherwise returns Err with the same Rc that was
        // passed in. Weak references are not considered. -> Result<T, Rc<T>>
       
        // ok - Converts from Result<T, E> to Option<T>. Consumes self, discards any error
        // -> Option<T>

        // into_inner - Consumes the RefCell, returning the wrapped value -> T
    }
}

// Pretty-printing
impl<T: Display> Display for List<T> {
    fn fmt(&self, w: &mut std::fmt::Formatter) -> std::result::Result<(), std::fmt::Error> {
        write!(w, "Snake length: {} {}", self.size, Paint::green("~<:|"))?;
        let mut node = self.head.clone();
        while let Some(n) = node {
            //print!("{} SC {}", n.borrow().data, Rc::strong_count(&n) - 1);
            write!(w, " {} ", Paint::blue(&n.borrow().data))?; //Note that the clone above increments the Strong Count
            node = n.borrow().right.clone();
            if node.is_some() {
                write!(w, "{}", Paint::green("|"))?;
            }
        }
        write!(w, "{}", Paint::green("|>"))
    }
}
