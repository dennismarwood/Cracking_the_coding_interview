use std::hash::{Hash, Hasher};
use std::cell::RefCell;
use std::rc::{Rc, Weak};
use std::cmp::Ordering;

pub type NodeRef<T> = Rc<RefCell<Node<T>>>;

pub struct NodeKey<T>(pub NodeRef<T>);

//Wanting to track when nodes are created (new) and drop (by impl drop for Node) but you cannot impl drop on a value that is moved.
//When we remove a node we are returning node.data. We are "moving" it / transferrring ownership to List. So we have "tracker". 
//Seems kinda hoaky to me. Alternative solutions could be implementing std::mem::replace or std::ptr::read but they are both "unsafe"
//and I think they are more than I want to bite off for now.
#[derive(Debug)]
pub struct Tracker{}

impl<T: PartialEq> PartialEq for NodeKey<T> {
    fn eq(&self, other: &Self) -> bool {
        self.0.borrow().data == other.0.borrow().data
    }
}

impl<T: Eq> Eq for NodeKey<T> {}

impl<T: std::hash::Hash > Hash for NodeKey<T> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.borrow().data.hash(state); 
    }
}

#[derive(Debug)]
pub struct Node<T> {
    pub data: T,
    pub left: Option<Weak<RefCell<Node<T>>>>,
    //A weak pointer to a RC pointer to the node. This node can mutate the left node. But it will see Option(None) if left node is deleted when it does upgrade.
    //We are able to mutate an object, via RC borrow, but only after we are forced to ensure that it is still there. Pretty nice. See the bottom of https://doc.rust-lang.org/book/ch15-06-reference-cycles.html?highlight=weak#visualizing-changes-to-strong_count-and-weak_count

    //The references with ownership are moving right down the chain towards the tail.
    pub right: Option<NodeRef<T>>,
    pub tracker: Tracker,
}

/*
impl<T> From<Node<T>> for i8 
where 
    T: Into<i8>
{
    fn from(node: Node<T>) -> Self {
        node.data.into()
    }
}
*/
impl<T: PartialEq> PartialEq for Node<T> {
    fn eq(&self, other: &Self) -> bool {
        self.data == other.data
    }
}

impl<T: PartialOrd> PartialOrd for Node<T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.data.partial_cmp(&other.data)
    }
}

impl<T: Ord> Ord for Node<T> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.data.cmp(&other.data)
    }
}

//Cannot use this. Also cannot use self.data in this
impl Drop for Tracker {
    fn drop(&mut self) {
        println!("Dropping Node");
    }
}


impl<T: Eq> Eq for Node<T> {}

impl<T: std::hash::Hash > Hash for Node<T> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.data.hash(state);
    }
}

impl<T: std::fmt::Debug + std::hash::Hash + std::cmp::Eq + std::cmp::PartialEq > Node<T> {

    // Constructs a node with some `data` initializing left and right to null.
    pub fn new(data: T) -> Self {
        Self { data, left: None, right: None, tracker: Tracker{} }
    }

    pub fn append(focus_node: &mut NodeRef<T>, data: T) -> Option<Rc<RefCell<Node<T>>>> {
        //Rust is doing some automatic dereferencing here.
        //https://doc.rust-lang.org/book/ch15-05-interior-mutability.html#having-multiple-owners-of-mutable-data-by-combining-rct-and-refcellt
        //https://doc.rust-lang.org/book/ch05-03-method-syntax.html#wheres-the---operator
        if focus_node.borrow().right.is_none() {
            let mut new_node = Node::new(data);
            new_node.left = Some(Rc::downgrade(&focus_node)); //Here is the weak ref. Set new node left pointer. The '&' gaining ref is automatically finding the RefCell pointer of the node, not creating a new ref.
            let rc = Rc::new(RefCell::new(new_node));
            focus_node.borrow_mut().right = Some(rc.clone());//borrow right again, this time mutable
            Some(rc)
        } else {
            if let Some( ref mut next_right) = focus_node.borrow_mut().right { //Sets the variable "next_right" See https://doc.rust-lang.org/stable/rust-by-example/scope/borrow/ref.html
                //for info on what ref does. It is used in pattern matching (for the "if let" here) to take references of the object. I think this stops some of rust's automatic dereferencing.
                Self::append(next_right, data)
            } else { None }
        }
    }

    pub fn insert_to_right_of_node(focus_node: &mut Rc<RefCell<Node<T>>>, data: T) -> Option<Rc<RefCell<Node<T>>>> {
        //New node
        let mut new_node = Node::new(data);

        new_node.left = Some(Rc::downgrade(&focus_node));
        new_node.right = focus_node.borrow().right.clone();

        let rc = Rc::new(RefCell::new(new_node));
        focus_node.borrow_mut().right = Some(rc.clone());

        if let Some(old_right) = rc.borrow().right.clone(){
            old_right.borrow_mut().left = Some(Rc::downgrade(&rc));
        }
        Some(rc)
    }

    pub fn remove_node(focus_node: Rc<RefCell<Node<T>>>) -> Option<T> {
        //Set node on left of focus node
        if let Some(left) = &focus_node.borrow().left {
            left.upgrade().unwrap().borrow_mut().right = focus_node.borrow().right.clone(); //Looks like upgrade is good for just this block
        }
        //Set node to the right of the focus node
        if let Some(right) = &focus_node.borrow().right {
            right.borrow_mut().left = focus_node.borrow().left.clone();
        }

        focus_node.borrow_mut().left = None;
        focus_node.borrow_mut().right = None;
        Some(Rc::try_unwrap(focus_node).ok()?.into_inner().data)
    }
}