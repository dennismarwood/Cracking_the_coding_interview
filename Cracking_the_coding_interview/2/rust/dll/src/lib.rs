//could have used #[path="lib/mod.rs"] mod lib; instead of chaning the name of src/lib folder.
pub mod my_lib;
use my_lib::list::List;

pub fn run() {
    let mut snake = List::new();
    
    snake.insert(1, 100);
    println!("{}", snake);
    snake.insert(2, 100);
    println!("{}", snake);
    snake.insert(3, 100);
    println!("{}", snake);

    for i in 1..3{
        snake.grow(i);
        //println!("{}", snake);
    }
    println!("{}", snake);
    for i in 1..3 {
        snake.push(i);
    }
    println!("{}", snake);
    snake.insert(1, 1);
    snake.insert(1, 5);
    snake.insert(1, 100);
    println!("{}", snake);
    snake.insert(9, 0);
    snake.insert(9, 100);
    println!("{}", snake);
    /*
    snake.insert(9, 100);
    println!("{}", snake);
    snake.insert(9, 5);
    println!("{}", snake);
    snake.insert(33, 110);
    snake.push(88);
    println!("{}", snake);
    */
    //println!("head strong count {}", Rc::strong_count(& snake.head.clone().unwrap() )); //This clone will increment the Strong count tainting the results.
    /*
    println!("SC for head {}", Rc::strong_count( &snake.head.as_ref().unwrap() ));
    println!("SC for tail {}", Rc::strong_count( &snake.tail.as_ref().unwrap() ));
    println!("head: {}", snake.head.clone().unwrap().borrow().data );
    println!("tail: {}", snake.tail.clone().unwrap().borrow().data );
    */
    snake.de_dup_with_buffer();
    snake.de_dup_without_buffer();
    //println!("{}", snake);
    //let my_i32: i32 = snake.pop_generic().into();
    //println!("{}", snake.pop().unwrap());
    //println!("{}", snake);
    //println!("{:?}", snake.pop());
    println!("{}", snake);
    snake.pop();
    println!("{}", snake);
    
}