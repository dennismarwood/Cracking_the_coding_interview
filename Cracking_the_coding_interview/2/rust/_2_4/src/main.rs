//Partition: Write code to partition a linked list around a value x, such that all nodes less than x come before all nodes greater
//than or equal to x. If x is contained within the list, the values of x only only need to be after the elements less than x
//(see below). The partition element x can appear anywhere in the "right partition"; it does not need to appear between the left
//and right partitions.

//EXAMPLE
//Input:  3->5->8->5->10->2->1 [partition = 5]
//Output: 3->1->2-10->5->5->8

use dll::my_lib::list::List;
use dll::my_lib::list::NodeRef;
use std::rc::Rc;
//asdf

pub trait ListExt<T> {
    fn sort_on(&mut self, partition: T);
    fn set_head(&mut self, target_node: NodeRef<T>);
}

impl<T: std::cmp::PartialOrd + std::fmt::Display> ListExt<T> for List<T> {
    //given a node look down the list and
    //shift node to head if it is < node.data
    fn sort_on(&mut self, partition: T) {
        let mut focus = self.head.clone();// increment RC count of head
        while let Some(f) = focus.clone() { //increment RC count of focus
            let next = f.borrow().right.clone(); // immutable borrow of f returning Refcell, node is implicitly derefed. next is ref to RC, increment RC count on next.right
            if f.clone().borrow().data < partition { //increment RC count of f/focus
                self.set_head(focus.unwrap());
            }
            focus = next;
        }
    }

    //given an existing node in the list, move it to the head position
    //pub type NodeRef<T> = Rc<RefCell<Node<T>>>;
    fn set_head(&mut self, target_node: NodeRef<T>){
        //node already at head
        if Rc::ptr_eq(&target_node, &self.head.clone().unwrap()) {
            return
        }
        //connect target's left and right to each other
        //move the left and right from target_node
        let right = target_node.try_borrow_mut().unwrap().right.take();
        let left =  target_node.try_borrow_mut().unwrap().left.take();

        //set the values of the left and right nodes of target to each other
        match right.clone() {
            Some(r) => {r.borrow_mut().left = left.clone()},
            None => {},
        }

        match left {
            Some(l) => {l.upgrade().clone().unwrap().borrow_mut().right = right.clone()},
            None => {},
        }
        
        //set the target's left and right and designate it as head
        target_node.borrow_mut().left = None;
        target_node.borrow_mut().right = self.head.clone();
        self.head = Some(target_node.clone());      
    }
}


fn main() {
    let mut snake = List::new();

    for i in 1..10{
        snake.grow(i);
    }
    snake.insert(6, 0);
    snake.insert(7, 3);
    snake.insert(8, 3);
    snake.insert(9, 4);
    snake.insert(10, 2);
    println!("{}", snake);
    println!("partion on 8");
    snake.sort_on(8);
    println!("{}", snake);
}
