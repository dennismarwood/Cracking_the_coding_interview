/*
Remove Dups: Write code to remove duplicates from an unsorted linked list.
FOLLOW UP
How would you solve this problem if a temporary buffer is not allowed?
*/

use dll::my_lib::list::List;
use dll::my_lib::node::Node;
use dll::my_lib::node::NodeKey;
use std::rc::{Rc};

//use std::fmt::Display;
use std::collections::HashSet;
/*
pub trait NodeEx {
    //fn remove_node(&self);
}

//#[derive(eq)]
impl<T: std::fmt::Debug + std::hash::Hash + std::cmp::Eq + std::cmp::PartialEq> NodeEx for Node<T> {

}

pub trait ListEx {
    fn print_head(&self);
    fn de_dup_with_buffer(&self);
}

impl<T: Display + std::fmt::Debug + std::hash::Hash + std::cmp::Eq> ListEx for List<T> {
    fn print_head(&self) {
        println!("head {}", self.head.clone().unwrap().borrow().data );
    }

    fn de_dup_with_buffer(&self) {
        
    }
}
*/
pub trait ListExt<T> {
    fn de_dup_with_bufferd(&mut self);
    fn de_dup_without_bufferd(&mut self);
}

impl<T: std::fmt::Debug + std::hash::Hash + std::cmp::Eq + std::cmp::PartialEq > ListExt<T> for List<T> {
    fn de_dup_without_bufferd(&mut self) {
        let mut focus = self.head.clone();
        while let Some(f) = focus {
            
            let mut runner = f.borrow().right.clone();
            while let Some(r) = runner {
                let next = r.borrow().right.clone();
                if f == r {
                    if self.tail.as_ref().map(|t| Rc::ptr_eq(&t, &r)).unwrap_or(false) {
                        self.tail = r.borrow().left.as_ref().and_then(|l| l.upgrade());
                    }
                    println!("Removed node with value: {:?}", Node::remove_node(r).unwrap());
                    self.size -= 1;
                }
                runner = next;
            }

            let next = f.borrow().right.clone();
            focus = next;
        }
    }
    
    fn de_dup_with_bufferd(&mut self) {
        let mut hashset = HashSet::new();
        let mut focus = self.head.clone();
        while let Some(f) = focus {
            let next = f.borrow().right.clone();
            if !hashset.insert(NodeKey(f.clone())) {
                //Check if we are about to try and remove node tail has strong count on. remove_node(node) will fail if target has >1 strong count on it.
                if self.tail.as_ref().map(|t| Rc::ptr_eq(&t, &f)).unwrap_or(false) {
                    self.tail = f.borrow().left.as_ref().and_then(|l| l.upgrade());
                }
                println!("Removed node with value: {:?}", Node::remove_node(f).unwrap());
                self.size -= 1;
            }
            focus = next;
        }
    }
}

fn main() {
    let mut snake = List::new();

    for i in 1..50{
        snake.grow(i);
    }
    println!("{}", snake);
    snake.insert(8, 0);
    snake.insert(8, 0);
    snake.insert(8, 0);
    snake.insert(8, 8);
    snake.insert(8, 88);
    println!("{}", snake);
    snake.de_dup_with_bufferd();
    println!("Deduped with buffer, now print\n{}", snake);

    for i in 35..50{
        snake.grow(i);
    }
    println!("{}", snake);
    snake.insert(8, 0);
    snake.insert(8, 0);
    snake.insert(8, 0);
    snake.insert(8, 8);
    snake.insert(8, 88);
    println!("{}", snake);
    snake.de_dup_without_bufferd();
    println!("Deduped without buffer, now print\n{}", snake);
}


